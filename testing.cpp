#define CATCH_CONFIG_MAIN
#include "catch (1).hpp"
#include "Book.h"
#include "binarytree.h"
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <stdio.h>
#include <sstream>
using namespace std;
typedef string treeType;



TEST_CASE("test1", "[Testinsert]")
{
  binarytree b;
  Book bb;
  //  Book* point = &bb;
  std::cout << "-----------------------------------------------------\n";
  std::cout << " 1st Test case (insert and search) " <<endl;
  std::cout << "-----------------------------------------------------";

  bb.setname("Mashaal Tareen");
  bb.settitle("Introduction to biotechnology");
  bb.setisbn("652019");
  bb.setqty( 20 );
  REQUIRE(b.Testinsert(bb) == true);
}



