#include <iostream>
#include<limits>
#include <fstream>
#include "binarytree.h"
#include "BOok.h"
using std::cout;
using std::cin;



int main()
{

  std::ofstream out;
  int savefile;
    binarytree b;
    int ch;
    string name;
    string key;
    string title;
    int qty;
    string isbn;
    Book tmp;
    Book tmp1;
    b.fillTree(&b);
    while(1)
    {
       cout<<endl<<endl;
       cout<<" Binary Search Tree Operations "<<endl;
       cout<<" --------------------------------------------- "<<endl;
       cout<<" 0. Search a book            "<<endl;
       cout<<" 1. Insert a record "<<endl;
       cout<<" 2. view textfile " << endl;
       cout<<" 3. Remove a record "<<endl;
       cout<<" 4. update the quantity  "<<endl;
       cout<<" 5. Exit and save file"<<endl;
       cout<<" Enter your choice : ";
       cin>>ch;

       
       if(ch == 0)
	 {
	       cout <<" Enter the name of the book to search for: "<<endl;
	       getline(std::cin >> std::ws, key);
	        b.search(key);
                 
	 }
       else if(ch == 1)
	 {
	       cout<<" Enter author name to be inserted: " << endl;
	       getline(std::cin >> std::ws, name);
		    
		    cout<<" Enter the title of the book: " << endl;
		    getline(std::cin >> std::ws, title);
		    cout<<" Enter the qty of the book: " << endl;
		    cin>>qty;
                    cout << endl << " Enter the isbn number: " << endl;
                    cin >> isbn;
                    tmp.setname(name);
		    tmp.settitle(title);
		    tmp.setqty(qty);
                    tmp.setisbn(isbn);
                    b.insert(tmp);

		    
	 }


       else if(ch == 2)
	 {
	     cout << endl;
	     b.view_file();
	 
	 }

		    
       else if(ch == 3)
	 {

	     cout<<" Enter book to be deleted : ";
	     getline(std::cin>> std::ws, key);
	     b.remove(key);
	 }

		    
       else if(ch == 4)
	 {
	     cout<<" Enter the name of the book whose qty you wish to change: " <<endl;
	     getline(std::cin>> std::ws, title) ;
                    cout<<endl<<" Enter the new qty: " <<endl;
                    cin>>qty;
                    cout<<endl;
                    b.changeqty(title, qty);
		 
	 }

       
       else if(ch == 5)
	 {
	     cout << "Do you want to save the changes to the text file? " << endl;
	     cout << "1- YES " << "\n" << "2- NO" << endl;
	     cin >>  savefile;

	     if (savefile == 1)
	       {
		 out.open("records.txt", ios:: trunc);
		    out <<  "\n";
                    out.close();
                    b.print_file();
	       }
	     else
	       {
		 cout << "Thankyou" << endl;
	       }

	     return 0;

	 }


    
       else
	 {
	   cout << " please enter a valid option. " << endl;
	 }

       
    }
}
