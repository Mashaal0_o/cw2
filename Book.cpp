#include "Book.h"


Book::Book()
{

}

Book::Book(string newname, string newtitle , int newqty, string newisbn)
{
    name = newname;
    title = newtitle;
    qty = newqty;
    isbn = newisbn;
}

string Book::getname() {
    return name;
}

string Book::gettitle() {
    return title;
}

int Book::getqty() {
  return qty;
}

string Book::getisbn() {
  return isbn;
}


void Book::setname(string newname) {
    name = newname;
}

void Book::settitle(string newtitle) {
  title = newtitle;
}

void Book::setqty(int newqty) {
  qty = newqty;
}

void Book::setisbn(string newisbn) {
  isbn = newisbn;
}
