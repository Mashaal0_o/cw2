

#include "binarytree.h"
using namespace std;

class Book;


// Smaller elements go left
// larger elements go right
void binarytree::insert(Book p)
{
  tree_node* temp = new tree_node; //new node is created
    tree_node* parent;
    temp->data = p; //data is transfered to the data part of the new node
    temp->left = NULL;
    temp->right = NULL;
    parent = NULL;

    // is this a new tree?
    if(isEmpty()) root = temp; //checks if tree is empty, if so then it is made the root of the tree
    else
      {




      
        //Note: ALL insertions are as leaf nodes
        // Find the Node's parent
	tree_node* current;
	current = root;
        while(current != NULL)
        {
            parent = current;
       if(temp->data.gettitle() > current->data.gettitle()) current = current->right; // compares the values and if the data entered is larger than curr it is pointed to the                right subtree or else left subtree 
            else current = current->left;
        }
	
	//now we connect the child node we added to its parent node
        if(temp->data.gettitle() < parent->data.gettitle())//if the title is smaller then, it os the left subchild 
           parent->left = temp;
        else
	  parent->right = temp;//else it is the right subchild
      }
    
}

void binarytree::remove(string p)
{

  Book b;
 
  
  tree_node *curr = search_new(p);//function to find the node to remove
  tree_node *parent = search_parent(p);//function to find the parent node to be removed
   
 

    // Node with single child

  if (curr == NULL || parent == NULL)//checks if either of the nodes needed arent found
    {
      cout << "try again " << endl;
    }
  else
    {
	
    if((curr->left == NULL && curr->right != NULL) || (curr->left != NULL
						       && curr->right == NULL))//confirms that either it has a right child or left
    {
      //right child is present, no left child
       if(curr->left == NULL && curr->right != NULL)
       {
	 if(parent->left == curr)//checks if the node to be deleted is the right child or left child of its parent
           {
             parent->left = curr->right;//if left, then parents left pointer is set to the left pointer of the node to be deleted
             delete curr;//node is del
           }
           else
           {
             parent->right = curr->right;//else the node is right child so, the right pointer is = to the right pointing node of curr
             delete curr;
           }
       }
       else // left child present, no right child
	 //same procedure as the above if function only difference is node has a left child instead
       {
          if(parent->left == curr)
           {
             parent->left = curr->left;
             delete curr;
           }
           else
           {
             parent->right = curr->left;
             delete curr;
           }
       }
     return;
    }

    //below if function confirms that the node doesnt have any children		 
    if( curr->left == NULL && curr->right == NULL)
    {
      if(parent->left == curr) parent->left = NULL;//here we just set either the right or left pointer to null that is pointing to curr
        else parent->right = NULL;
	delete curr;
        return;
    }


    //Node with 2 children
    // replace node with smallest value in right subtree
    if (curr->left != NULL && curr->right != NULL)
    {
        tree_node* chkr;
        chkr = curr->right;
        if((chkr->left == NULL) && (chkr->right == NULL))
        {
            curr = chkr;
            delete chkr;
            curr->right = NULL;
        }
        else // right child has children
        {
            //if the node's right child has a left child
            // Move all the way down left to locate smallest element

            if((curr->right)->left != NULL)
            {
                tree_node* lcurrent;
                tree_node* lcurrparent;
                lcurrparent = curr->right;
                lcurrent = (curr->right)->left;
                while(lcurrent->left != NULL)//this loop keeps going until the last element isnt found in the left subtree
                {
                   lcurrparent = lcurrent;
                   lcurrent = lcurrent->left;
                }
		curr->data = lcurrent->data;
                delete lcurrent;
                lcurrparent->left = NULL;
           }
           else
           {
               tree_node* tmp;
               tmp = curr->right;
               curr->data = tmp->data;
	       curr->right = tmp->right;
               delete tmp;
           }

	    

        }
	  
		 return;
    }
    }

}
	





void binarytree::print_file()
{//this function is used to send a root as a parimeter to access the binary tree
  WriteToFile(root);
  }
void binarytree::view_file()
{//this functiion is used to send the root as a parameter to access the binary tree
  viewfile(root);
}
    



void binarytree::search(string key)
{
     bool found = false;
    if(isEmpty())
    {
        cout<<" This Tree is empty! "<<endl;
        return;
    }

    tree_node* curr;
    curr = root;

    while(curr != NULL)
    {
      if(curr->data.gettitle() == key)//keeps comparing the nodes title until it doesnt matches the key
         {
            found = true;
	    cout << "the autor for "<<key << " is " << curr->data.getname()<< endl;
            cout << "The qty for " << key << " is " << curr->data.getqty() << endl;
	    cout << "the isbn for "<< key << " is " << curr->data.getisbn() << endl;
            break;
         }
         else
	   {//if match isnt found, compares the values and if the key is greatter than we look in the right subtree else the left  
             if(key>curr->data.gettitle()) curr = curr->right;
             else curr = curr->left;
         }
    }
    if(!found)
		 {
        cout<<" Data not found! "<<endl;
        return;
    }
}

binarytree::tree_node * binarytree::search_new(string key)
{// same as the function above only change is it returns a pointer to the node found

  tree_node * node;
    bool found = false;
    if(isEmpty())
    {
        cout<<" This Tree is empty! "<<endl;
        node = NULL;
    }

    tree_node* curr;
    curr = root;

    while(curr != NULL)
    {
         if(curr->data.gettitle() == key)
         {
            found = true;
	    node = curr;
            break;
         }
         else
         {
             if(key>curr->data.gettitle()) curr = curr->right;
             else curr = curr->left;
         }
    }
      if(!found)
                 {
        cout<<" search not found! "<<endl;
        node = NULL;
    }

      return node;
}

binarytree::tree_node * binarytree::search_parent(string key)
{//same as the above functio, but it returns a pointer to the parent of the node being search for

  tree_node * node;
    bool found = false;
    if(isEmpty())
    {
        cout<<" This Tree is empty! "<<endl;
        node = NULL;
    }

    tree_node* curr;
    curr = root;

    while(curr != NULL)
    {
         if(curr->data.gettitle() == key)
         {
            found = true;
            break;
         }
         else
	   {//the previos iteration is marked as the parent, when the node is found 
	   tree_node *parent = curr;
	   node = parent;
             if(key>curr->data.gettitle()) curr = curr->right;
             else curr = curr->left;
         }
    }
      if(!found)
                 {
        cout<<" parent not found! "<<endl;
        node = NULL;
    }

      return node;
}





void binarytree::changeqty(string p, int newwqty) {
  

  tree_node *curr = search_new(p);//we search the node using the key

   if(curr == NULL)
     {
       cout << "please enter the correct title" << endl;
     }
   else
     {
   //change the qty associated with the node
    curr->data.setqty(newwqty);//here we just replace the old quantity with the new one
    cout<< "qty changed successfully. " << endl;
     }
}


void binarytree :: fillTree( binarytree *b)
{
    std::ifstream file;
    file.open("records.txt");
    if(!file) {
        cout<<" Error opening file. " << endl;
    }
    //variables used to load data into the tree
    string name;
    string title;
    string temp;
    int qty;
    string isbn;
    Book p;
    string s; 
    std::string line ;
    
     while (true)
    {
      if (file.eof())
	{
	  break;
	}
      else
	{
      while( std::getline(file,line) )
      {
	  std::stringstream ss(line);

	  
	  std::getline(ss,title,'\t'); p.settitle(title);    
	  std::getline(ss,name,'\t'); p.setname(name);
	  std::getline(ss,isbn,'\t'); p.setisbn(isbn);
	  std::getline(ss,temp,'\t'); stringstream degree(temp); degree >> qty; p.setqty(qty);
        
     
        cout << p.gettitle() << " " << p.getname() << " " << p.getisbn() << " " << p.getqty() << endl;
	(*b).insert(p);
      }
	}

    }


    file.close();
}


void binarytree :: viewfile( tree_node *p)
{
  //this function is to view the binaytree inorder manner
    
  if(p != NULL)
    {
      if(p->left) viewfile(p->left); //checks if p has a left pointer, if yes it is passed as the parameter again into the function
      cout <<p->data.gettitle()<< "\t" << p->data.getname() << "\t" << p->data.getisbn() << "\t" << p->data.getqty() << endl;//prints p's data
      if(p->right) viewfile(p->right);//checks if p has a right pointer,if yes it is passed as parameter until it doesnt reaches null 
    }
      else return;
  
}


void binarytree :: WriteToFile(tree_node* p)
{
  std::ofstream out;
  out.open("records.txt", std::ios::app);//opens file in append formate
  if(p != NULL)//untill p isnt null, write all the nodes inorder into the txt file
    {
        if(p->left) WriteToFile(p->left);
        out <<p->data.gettitle()<< "\t" << p->data.getname() << "\t" << p->data.getisbn() << "\t" << p->data.getqty() << endl;
        if(p->right) WriteToFile(p->right);
	out.close();
    }
      else return;
      }

//-----------------------------------------------------------------------------------------------------------------------------

//TESTING FUNCTIONS//

bool binarytree::Testinsert(Book p)
{
  //bool done;
  tree_node* t = new tree_node; //new node is created
    tree_node* parent;
    t->data = p; //data is transfered to the data part of the new node
    t->left = NULL;
    t->right = NULL;
    parent = NULL;
    string key = t->data.gettitle();
    //    string key = t->data.gettitle();

    // is this a new tree?
    if(isEmpty()) root = t;  //checks if tree is empty, if so then it is made the root of the tree
    else
      {




      
        //Note: ALL insertions are as leaf nodes
        // Find the Node's parent
	tree_node* curr;
	curr = root;
        while(curr != NULL)
        {
            parent = curr;
            if(t->data.gettitle() > curr->data.gettitle()) curr = curr->right; // compares the values and if the data entered is larger than curr it is pointed to the                right subtree or else left subtree 
            else curr = curr->left;
        }
	//done = true;
	
	//now we connect the child node we added to its parent node
        if(t->data.gettitle() < parent->data.gettitle())//if the title is smaller then, it os the left subchild 
           parent->left = t;
        else
	  parent->right = t;//else it is the right subchild
      }

    //    return true;


    //---------------------------------insertion complete------------------------------------

    //now it searches for the record and displays all information if found
    
    tree_node* chkr;
    chkr = root;
    bool found = false;

    while(chkr != NULL)
    {
         if(chkr->data.gettitle() == key)
         {
	   found = true;
	    cout << "the autor for "<<key << " is " << chkr->data.getname()<< endl;
            cout << "The qty for " << key << " is " << chkr->data.getqty() << endl;
	    cout << "the isbn for "<< key << " is " << chkr->data.getisbn() << endl;
            break;
	    // return true;
         }
         else
         {
             if(key>chkr->data.gettitle()) chkr = chkr->right;
             else chkr = chkr ->left;
         }
    }
    if(!found)
		 {
        cout<<" Data not found! "<<endl;
        
    }
    //return found;
    if(found == true)
      {
	return true;
      }
    else
      return false;
    
}





  


 


      

